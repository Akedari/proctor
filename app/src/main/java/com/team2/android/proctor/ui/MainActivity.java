package com.team2.android.proctor.ui;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.team2.android.proctor.R;
import com.team2.android.proctor.util.SessionManager;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ((Proctor)getApplicationContext()).getSession().checkUserType();
    }
}
