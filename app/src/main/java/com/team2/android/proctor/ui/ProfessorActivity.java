package com.team2.android.proctor.ui;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.team2.android.proctor.R;
import com.team2.android.proctor.model.constants.Constants;
import com.team2.android.proctor.model.output.Course;
import com.team2.android.proctor.util.CourseAdapter;
import com.team2.android.proctor.util.FlyOutContainer;
import com.team2.android.proctor.util.JSONParser;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.sql.Time;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class ProfessorActivity extends AppCompatActivity {
    FlyOutContainer root;
    public ArrayList<Course> courses  = new ArrayList<Course>();
    Proctor proctor;
    CourseAdapter courseAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.root = (FlyOutContainer) this.getLayoutInflater().inflate(R.layout.activity_student,null);
        setContentView(root);
        //setContentView(R.layout.activity_professor);

        proctor = (Proctor) getApplicationContext();

        ListView courselist = (ListView) findViewById(R.id.courselist);


        Intent intent = getIntent();
        long professorIDint = intent.getLongExtra("professorId",0);
        String professorID = String.valueOf(professorIDint);
        GetProfessorCourseData getProfessorCourseData =  new GetProfessorCourseData(professorID);
        getProfessorCourseData.execute((Void) null);

        JSONArray jsonCourses = null;
        try {
            jsonCourses = getProfessorCourseData.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }


        int size;

        size = jsonCourses.length();


        String[] courseNames = new String[size];
        Time[] stimestamps = new Time[size];
        Time[] etimestamps = new Time[size];
        String[] days = new String[size];
        int[] courseIds = new int[size];

        try {
            for(int i=0;i<size;i++)
            {
                JSONObject object = jsonCourses.getJSONObject(i);
                courseNames[i]=object.getString("courseName");
                stimestamps[i]= Time.valueOf(object.getString("courseStartTime"));
                etimestamps[i]= Time.valueOf(object.getString("courseEndTime"));
                days[i]=object.getString("days");
                courseIds[i] = object.getInt("course_id");

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Course coursearray[] = new Course[size];
        for(int i=0;i<size;i++)
        {
            coursearray[i] = new Course(courseIds[i],courseNames[i],
                    stimestamps[i],etimestamps[i],days[i]);
            courses.add(coursearray[i]);
        }

        courseAdapter = new CourseAdapter(this,android.R.layout.simple_list_item_1,courses);
        courselist.setAdapter(courseAdapter);

        courselist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                //start attendance activity with selected course details and user type
                Course course = courses.get(position);
                Intent intent = new Intent(ProfessorActivity.this,AttendanceActivity.class);

               intent.putExtra("course",course);
                intent.putExtra("user",0);
                startActivity(intent);
            }
        });

    }

    public void toggleMenu(View v){
        this.root.toggleMenu();
    }

    public void logout(View v){
        proctor.getSession().logoutUser();
    }

    public void helppage(View v){
        Intent i = new Intent(this, HelpActivity.class);
        startActivity(i);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_professor, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.logout) {
            proctor.getSession().logoutUser();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }



    class GetProfessorCourseData extends AsyncTask<Void, Void, JSONArray> {
        JSONObject sendJsonObject;
        private final String id;

        GetProfessorCourseData(String id)
        {
            this.id=id;
        }

        @Override
        protected JSONArray doInBackground(Void... params) {
            JSONArray returnJsonVal = null;

            List<NameValuePair> params1 = new ArrayList<NameValuePair>();

            params1.add(new BasicNameValuePair("id",id));

            returnJsonVal = JSONParser.makeHttpRequestArray(Constants.PROFESSOR_COURSES_URL, "POST", params1);

            return returnJsonVal;

        }

//        @Override
//        protected void onPostExecute() {
//            super.onPostExecute();
//            //intent to join student or professor activity
//        }
    }

}
