package com.team2.android.proctor.ui;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.IntDef;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.nearby.Nearby;
import com.google.android.gms.nearby.connection.AppIdentifier;
import com.google.android.gms.nearby.connection.AppMetadata;
import com.google.android.gms.nearby.connection.Connections;
import com.google.android.gms.nearby.connection.ConnectionsStatusCodes;
import com.team2.android.proctor.R;
import com.team2.android.proctor.model.constants.Constants;
import com.team2.android.proctor.model.input.Attendance;
import com.team2.android.proctor.model.input.LoginInput;
import com.team2.android.proctor.model.output.Course;
import com.team2.android.proctor.util.JSONParser;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.util.ArrayList;
import java.util.List;

//import com.team2.android.proctor.model.Course;

public class AttendanceActivity extends AppCompatActivity implements
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        View.OnClickListener,
        Connections.ConnectionRequestListener,
        Connections.MessageListener,
        Connections.EndpointDiscoveryListener {


    private static final String TAG = AttendanceActivity.class.getSimpleName();
    SharedPreferences sharedPref;
    Long pref_UserId;
    Course course;
    Proctor proctor;

    /**
     * Timeouts (in millis) for startAdvertising and startDiscovery.
     */
    private static final long TIMEOUT_ADVERTISE = 1000L * 30L;
    private static final long TIMEOUT_DISCOVER = 1000L * 30L;

    /**
     * Possible states for this application:
     *      IDLE - GoogleApiClient not yet connected, can't do anything.
     *      READY - GoogleApiClient connected, ready to use Nearby Connections API.
     *      ADVERTISING - advertising for peers to connect.
     *      DISCOVERING - looking for a peer that is advertising.
     *      CONNECTED - found a peer.
     */
    @Retention(RetentionPolicy.CLASS)
    @IntDef({STATE_IDLE, STATE_READY, STATE_ADVERTISING, STATE_DISCOVERING, STATE_CONNECTED})
    public @interface NearbyConnectionState {}
    private static final int STATE_IDLE = 1023;
    private static final int STATE_READY = 1024;
    private static final int STATE_ADVERTISING = 1025;
    private static final int STATE_DISCOVERING = 1026;
    private static final int STATE_CONNECTED = 1027;

    private GoogleApiClient mGoogleApiClient;


    /** The current state of the application **/
    @NearbyConnectionState
    private int mState = STATE_IDLE;

    /** Views and dialogs**/
    TextView course_tv;
    TextView duration_tv;
    Button take_attendance_btn;
    Button view_attendance_btn;
    Button give_attendance_btn;

    /** The endpoint ID of the connected peer, used for messaging **/
    private String mOtherEndpointId;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        proctor = (Proctor) getApplicationContext();

        Intent i = getIntent();

        int userType = i.getIntExtra("user", 9999);
        sharedPref = getApplicationContext().getSharedPreferences(getString
                (R.string.SHARED_PREF_KEY), Context.MODE_PRIVATE);
        pref_UserId=sharedPref.getLong(getString(R.string.USERID), 9999);

        Log.d(TAG,"the user id :"+ pref_UserId);

         course = (Course) i.getSerializableExtra("course");

        //set view based on user type 0-prof, 1-student
        setContentView(userType == 0 ? R.layout.activity_prof_attendance : R.layout.activity_student_attendance);

        course_tv = (TextView) findViewById(R.id.course_tv);
        duration_tv = (TextView) findViewById(R.id.duration_tv);
        take_attendance_btn = (Button) findViewById(R.id.takeatt_btn);
        view_attendance_btn = (Button) findViewById(R.id.viewatt_btn);
        give_attendance_btn = (Button) findViewById(R.id.checkin_btn);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        /*course_tv.setText(course.getCourseName());
        duration_tv.setText(course.getStartDuration().toString()+course.getEndDuration().toString());*/

        course_tv.setText(course.getCourseName());
        duration_tv.setText(course.getCourseStartTime()+" to "+ course.getCourseEndTime());

        // Initialize Google API Client for Nearby Connections.
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(Nearby.CONNECTIONS_API)
                .build();

        if (take_attendance_btn != null) take_attendance_btn.setOnClickListener(this);
        if (view_attendance_btn != null) view_attendance_btn.setOnClickListener(this);

        if (give_attendance_btn != null) give_attendance_btn.setOnClickListener(this);

    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d(TAG, "onStart");
        mGoogleApiClient.connect();
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d(TAG, "onStop");

        // Disconnect the Google API client and stop any ongoing discovery or advertising. When the
        // GoogleAPIClient is disconnected, any connected peers will get an onDisconnected callback.
        if (mGoogleApiClient != null) {
            mGoogleApiClient.disconnect();
        }
    }

    /**
     * Check if the device is connected (or connecting) to a WiFi network.
     * @return true if connected or connecting, false otherwise.
     */
    private boolean isConnectedToNetwork() {
        ConnectivityManager connManager = (ConnectivityManager)
                getSystemService(CONNECTIVITY_SERVICE);
        NetworkInfo info = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

        return (info != null && info.isConnectedOrConnecting());
    }

    /**
     * Begin advertising for Nearby Connections, if possible.
     */
    private void startAdvertising() {
        Log.d(TAG,"start advertising");
        if (!isConnectedToNetwork()) {
            Log.d(TAG,"startAdvertising: not connected to WiFi network.");
            //send a signal to service
            return;
        }

        List<AppIdentifier> appIdentifierList = new ArrayList<>();
        appIdentifierList.add(new AppIdentifier(getPackageName()));
        AppMetadata appMetadata = new AppMetadata(appIdentifierList);

        // Advertise for Nearby Connections.
        String name = null;
        Nearby.Connections.startAdvertising(mGoogleApiClient, name, appMetadata, TIMEOUT_ADVERTISE,
                this).setResultCallback(new ResultCallback<Connections.StartAdvertisingResult>() {

            @Override
            public void onResult(Connections.StartAdvertisingResult result) {
                Log.d(TAG, "startAdvertising:onResult:" + result);

                if (result.getStatus().isSuccess()) {
                    Log.d(TAG, "startAdvertising:onResult: SUCCESS");
                    updateToast(STATE_ADVERTISING);
                } else {
                    Log.d(TAG, "startAdvertising:onResult: FAILURE");

                    int statusCode = result.getStatus().getStatusCode();
                    if (statusCode == ConnectionsStatusCodes.STATUS_ALREADY_ADVERTISING) {
                        Log.d(TAG, "STATUS_ALREADY_ADVERTISING");
                    } else {
                        Log.d(TAG, "STATE_READY");
                        updateToast(STATE_READY);
                    }
                }
            }
        });
    }

    private void startDiscovery() {
        Log.d(TAG, "startDiscovery");
        if (!isConnectedToNetwork()) {
            Log.d(TAG, "startDiscovery: not connected to WiFi network.");

            //TODO: backup for WIFI
            //if prof sent signal to service
            //then send attendance to service directly
            return;
        }

        // Discover nearby apps that are advertising with the required service ID.
        String serviceId = getString(R.string.service_id);
        Nearby.Connections.startDiscovery(mGoogleApiClient, serviceId, TIMEOUT_DISCOVER, this)
                .setResultCallback(new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                        if (status.isSuccess()) {
                            Log.d(TAG, "startDiscovery:onResult: SUCCESS");

                            updateToast(STATE_DISCOVERING);
                        } else {
                            Log.d(TAG, "startDiscovery:onResult: FAILURE");

                            // If the user hits 'Discover' multiple times in the timeout window,
                            // the error will be STATUS_ALREADY_DISCOVERING
                            int statusCode = status.getStatusCode();
                            if (statusCode == ConnectionsStatusCodes.STATUS_ALREADY_DISCOVERING) {
                                Log.d(TAG, "STATUS_ALREADY_DISCOVERING");
                            } else {
                                updateToast(STATE_READY);
                            }
                        }
                    }
                });
    }

    private void sendMessage() {
        // Sends a reliable message, which is guaranteed to be delivered eventually and to respect
        // message ordering from sender to receiver.

        //send id:present
        String msg = ""+pref_UserId+":"+ course.getCourse_id();
        Log.d(TAG,"sending message...");
        Nearby.Connections.sendReliableMessage(mGoogleApiClient, mOtherEndpointId, msg.getBytes());
    }

    /**
     * Change the application state and update the visibility on on-screen views '
     * based on the new state of the application.
     * @param newState the state to move to (should be NearbyConnectionState)
     */
    private void updateToast(@NearbyConnectionState int newState) {
        mState = newState;
        switch (mState) {
            case STATE_IDLE:
                // The GoogleAPIClient is not connected, we can't yet start advertising
                Toast.makeText(AttendanceActivity.this, "Can't start advertising",
                        Toast.LENGTH_SHORT).show();
                break;
            case STATE_READY:
                // The GoogleAPIClient is connected, we can begin advertising or discovery.
                Toast.makeText(AttendanceActivity.this, "Can start advertising or discovery",
                        Toast.LENGTH_SHORT).show();
                break;
            case STATE_ADVERTISING:
                break;
            case STATE_DISCOVERING:
                break;
            case STATE_CONNECTED:
                // We are connected to another device via the Connections API
                Toast.makeText(AttendanceActivity.this, "Connected to another device",
                        Toast.LENGTH_SHORT).show();
                break;
        }
    }



    /**
     * Send a connection request to a given endpoint.
     * @param endpointId the endpointId to which you want to connect.
     * @param endpointName the name of the endpoint to which you want to connect. Not required to
     *                     make the connection, but used to display after success or failure.
     */
    private void connectTo(String endpointId, final String endpointName) {
        Log.d(TAG,"connectTo:" + endpointId + ":" + endpointName);

        // Send a connection request to a remote endpoint.
        String myName = null;
        byte[] myPayload = null;
        Nearby.Connections.sendConnectionRequest(mGoogleApiClient, myName, endpointId, myPayload,
                new Connections.ConnectionResponseCallback() {
                    @Override
                    public void onConnectionResponse(String endpointId, Status status,
                                                     byte[] bytes) {
                        Log.d(TAG, "onConnectionResponse:" + endpointId + ":" + status);
                        if (status.isSuccess()) {
                            Log.d(TAG, "onConnectionResponse: " + endpointName + " SUCCESS");
                            Toast.makeText(AttendanceActivity.this, "Connected to " + endpointName,
                                    Toast.LENGTH_SHORT).show();

                            mOtherEndpointId = endpointId;
                            updateToast(STATE_CONNECTED);
                            //send message now
                            sendMessage();
                        } else {
                            Log.d(TAG, "onConnectionResponse: " + endpointName + " FAILURE");
                        }
                    }
                }, this);
    }


    @Override
    public void onConnected(Bundle bundle) {
        Log.d(TAG,"onConnected");
        updateToast(STATE_READY);
    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.d(TAG,"onConnectionSuspended: " + i);
        updateToast(STATE_IDLE);

        // Try to re-connect
        mGoogleApiClient.reconnect();
    }

    @Override
    public void onConnectionRequest(final String endpointId, String deviceId, String endpointName,
                                    byte[] payload) {
        Log.d(TAG,"onConnectionRequest:" + endpointId + ":" + endpointName);


        Nearby.Connections.acceptConnectionRequest(mGoogleApiClient, endpointId,
                payload, AttendanceActivity.this)
                .setResultCallback(new ResultCallback<Status>() {
                    @Override
                    public void onResult(Status status) {
                        if (status.isSuccess()) {
                            Log.d(TAG, "acceptConnectionRequest: SUCCESS");

                            mOtherEndpointId = endpointId;
                            updateToast(STATE_CONNECTED);
                        } else {
                            Log.d(TAG, "acceptConnectionRequest: FAILURE");
                        }
                    }
                });

    }

    @Override
    public void onEndpointFound(final String endpointId, String deviceId, String serviceId,
                                final String endpointName) {

        Log.d(TAG, "onEndpointFound:" + endpointId + ":" + endpointName);

        AttendanceActivity.this.connectTo(endpointId,endpointName);

    }

    @Override
    public void onEndpointLost(String endpointId) {
        Log.d(TAG,"onEndpointLost:" + endpointId);
    }

    @Override
    public void onMessageReceived(String endpointId, byte[] payload, boolean isReliable) {
        //send payload in async task
        Log.d(TAG,"onMessageReceived:" +  new String(payload));
        String message = new String(payload);
        Attendance attendance = new Attendance();
        attendance.setUserId(Long.parseLong(message.split(":")[0]));
        attendance.setCourseId(Integer.parseInt(message.split(":")[1]));

        new TakeAttendanceTask().execute(attendance);
    }

    @Override
    public void onDisconnected(String endpointId) {
       Log.d(TAG,"onDisconnected:" + endpointId);
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.viewatt_btn:
                //call to view Attendance activity
               Intent intent = new Intent(this,ViewAttendanceActivity.class);
                intent.putExtra("courseId",course.getCourse_id());
                startActivity(intent);
                break;

            case R.id.takeatt_btn:
                startAdvertising();
                new AlertDialog.Builder(this)
                        .setTitle("Attendance")
                        .setMessage("Taking attendance...")
                        .setCancelable(true).show();
                break;
            case R.id.checkin_btn:
                startDiscovery();
                new AlertDialog.Builder(this)
                        .setTitle("Attendance")
                        .setMessage("Giving attendance...")
                        .setCancelable(true).show();

                break;

        }
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.d(TAG,"onConnectionFailed: " + connectionResult);
        updateToast(STATE_IDLE);
    }


    class TakeAttendanceTask extends AsyncTask<Attendance, Void, JSONObject>{

        @Override
        protected JSONObject doInBackground(Attendance... params) {
            JSONObject returnJsonVal = null;
            for (Attendance param : params) {
                List<NameValuePair> params1 = new ArrayList<>();

                params1.add(new BasicNameValuePair("courseId",
                        String.valueOf(param.getCourseId())));
                params1.add(new BasicNameValuePair("userId",
                        String.valueOf(param.getUserId())));

                returnJsonVal = JSONParser.makeHttpRequest(Constants.TAKE_ATTENDANCE_URL, "POST", params1);
            }
            return returnJsonVal;
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {
            super.onPostExecute(jsonObject);

            try {
                String status = jsonObject.getString("response");
                Log.d(TAG,"response: "+ status);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

}
